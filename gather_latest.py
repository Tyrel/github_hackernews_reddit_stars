from get_new_hn import hacker_news
from pymongo import MongoClient
import datetime


def get_new_hackernews():
    client = MongoClient()
    hn_data = hacker_news()
    hn_db = client.githubs.hacker_news

    inserted = 0
    for hn in hn_data:
        if not hn_db.find_one({'url': hn['url']}):
            hn_db.insert(hn)
            inserted += 1
    print datetime.datetime.now(), "Inserted", inserted


if __name__ == '__main__':
    get_new_hackernews()